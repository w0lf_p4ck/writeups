# Easy Peasy 

[link](https://crackmes.one/crackme/5d295dde33c5d410dc4d0d05)

If you've looked at the link already, your'll see this is a windows one this time. :/ 

After downloading the `.zip` file, and extracting it, first thing first is to see what we are working with.

```
PS C:\Users\jthorpe\Downloads> .\EasyPeasy.exe
Please, login with your credentials.
Username:test
GTFO you lame ass hacker.
```

So I then decided to open the `.exe` in `Cutter` 

```
..\opt\cutter\cutter.exe .\EasyPeasy.exe
```

`Cutter` is a nice GUI front end to `radare2` and as i'm on windows this time, I'll do a lot more point and click.

Selecting the `sym.main` function within `Cutter` I can see the disassembly of that function, however if i click "Windows->Decompiler" i can see somewhat C style code, because `Cutter` leverages a `Ghidra` decompiler.

So having almost source for the main function, the intresting parts are here.

```
    __main();
    std::allocator<char>::allocator()(&uStack26);
    
    std::basic_string<char, std::char_traits<char>, std::allocator<char> >::basic_string(char const*, std::allocator<char> const&)
              (unaff_RSI, (int64_t)"iwonderhowitfeelstobeatimetraveler", (int64_t)auStack40, (int64_t)&uStack26);
    std::allocator<char>::~allocator()(&uStack26);
    std::allocator<char>::allocator()(auStack25);
    
    std::basic_string<char, std::char_traits<char>, std::allocator<char> >::basic_string(char const*, std::allocator<char> const&)
              (unaff_RSI, (int64_t)"heyamyspaceboardisbrokencanyouhelpmefindit?", (int64_t)auStack56, (int64_t)auStack25
              );
```

Theres two variables defined there, and if i use them as a username/password in the binary, it works !!

```
PS C:\Users\jthorpe\Downloads> .\EasyPeasy.exe
Please, login with your credentials.
Username:iwonderhowitfeelstobeatimetraveler
Now, please insert the password.
Password:heyamyspaceboardisbrokencanyouhelpmefindit?
You have successfully logged into the system.
PS C:\Users\jthorpe\Downloads>
```